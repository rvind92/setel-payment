# Payment application - Setel Assignment

This microservice is responsible for receiving and generating a payment response (mock payment result) from the Orders application. 

The solution is deployed on AWS Elastic Beanstalk in the NodeJS environment.

## Stack
- NodeJS + ExpressJS
- AWS Elastic Beanstalk

## Running the application

1. Clone or download the repository, navigate into it and run the npm install command
```
git clone
cd setel-payment
npm install
```
2. To start the application, run the following command
```
npm start
```
3. To run the tests, run the following command
```
npm test
```
