const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const PORT = process.env.PORT || 3002
const states = [ 'CONFIRMED', 'CANCELLED' ]

app.set('port', PORT)
app.use(bodyParser.json())

app.post('/', (request, response) => {
  const { orderId } = request.body
  const state = states[Math.floor(Math.random() * states.length)]
  const delay = Math.random() * Math.floor(8) * 1000
  const data = { orderId, state }
  setTimeout(() => {
    return response.status(200).json(data)
  }, delay)
})

app.listen(PORT, () => console.log(`Orders app is running at port ${PORT}`))
