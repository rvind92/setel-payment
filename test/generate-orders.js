const mocha = require('mocha')
const faker = require('faker')
const chai = require('chai')
const { assert } = chai

describe('Generating random state for order', () => {
  const order = {}
  beforeEach(() => {
    order.name = faker.name.findName()
    order.description = faker.lorem.sentence()
    order.status = 'CREATED'
  })

  // Test will pass if we return an order in CONFIRMED state
	it('should return order in CONFIRMED state', (done) => {
    setTimeout(() => {
      order.status = 'CONFIRMED'
      assert.equal(order.status, 'CONFIRMED')
      done()
    }, 750)
	})

  // Test will pass if we return an order in CANCELLED state
  it('should return order in CANCELLED state', (done) => {
    setTimeout(() => {
      order.status = 'CANCELLED'
      assert.equal(order.status, 'CANCELLED')
      done()
    }, 750)
	})
})
